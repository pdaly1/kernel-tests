summary: Confirm watchdog system call fuzz testing.
description: |
  Confirm system call fuzz testing is being performed on watchdog system calls specified in Clocks and Timers Risk Assessment.
  The database is checked at the end to ensure each syscall was executed at least once.
  Default runtime is 1 hour (3600 seconds), but can be changed using the "timer" parameter.
  Test inputs:
      A list with the system calls to be fuzzed, and a fuzzer program
      called syzkaller. Input details:
      - System call list from RA documentation.
      - syzkaller code: https://github.com/google/syzkaller
      - check for execution: grep -q "^${syscall}[$,(]" "${local_dir}"/corpus_dir/*
  Expected results:
      If the system calls are executed without incident during the test runtime,
      the following output should be expected:
      [   LOG    ] :: Test duration was 3602 seconds.
      [   PASS   ] :: No crash results found.
      [   PASS   ] :: ioctl$WDIOC_GETSUPPORT executed. 
      [   PASS   ] :: ioctl$WDIOC_GETSTATUS executed. 
      [   PASS   ] :: ioctl$WDIOC_GETBOOTSTATUS executed. 
      [   PASS   ] :: ioctl$WDIOC_GETTEMP executed. 
      [   PASS   ] :: ioctl$WDIOC_SETOPTIONS executed. 
      [   PASS   ] :: ioctl$WDIOC_KEEPALIVE executed. 
      [   PASS   ] :: ioctl$WDIOC_SETTIMEOUT executed. 
      [   PASS   ] :: ioctl$WDIOC_GETTIMEOUT executed. 
      [   PASS   ] :: ioctl$WDIOC_SETPRETIMEOUT executed. 
      [   PASS   ] :: ioctl$WDIOC_GETPRETIMEOUT executed. 
      [   PASS   ] :: ioctl$WDIOC_GETTIMELEFT executed. 
  Results location:
      output.txt
contact: Pablo Ridolfi <pridolfi@redhat.com>
component:
  - kernel
test: bash ./runtest.sh
path: /misc/watchdog-fuzzing-test
id: d20d1992-f989-44e8-a501-d814a516a0a6
framework: beakerlib
require:
  - make
  - golang
  - gcc
  - glibc
  - glibc-common
  - glibc-devel
  - gcc-c++
  - wget
  - git
  - patch
  - type: file
    pattern:
        - /kernel-include
        - /memory/mmra/syzkaller
environment:
    mm_syscalls: '"ioctl$WDIOC_GETSUPPORT", "ioctl$WDIOC_GETSTATUS", "ioctl$WDIOC_GETBOOTSTATUS", "ioctl$WDIOC_GETTEMP", "ioctl$WDIOC_SETOPTIONS", "ioctl$WDIOC_KEEPALIVE", "ioctl$WDIOC_SETTIMEOUT", "ioctl$WDIOC_GETTIMEOUT", "ioctl$WDIOC_SETPRETIMEOUT", "ioctl$WDIOC_GETPRETIMEOUT", "ioctl$WDIOC_GETTIMELEFT"'
    supportcalls: '"syz_open_dev$watchdog"'
    git_patches: '../../../../misc/watchdog-fuzzing-test/dev_watchdog.patch'
duration: 2h
